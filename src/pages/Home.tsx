import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar
  ,IonItemDivider,IonList,IonItem,IonInput,IonLabel,IonRouterOutlet ,IonButton ,IonButtons,IonBackButton,   
} from '@ionic/react';
import { Redirect, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';

import './Home.css';
import { Plugins } from '@capacitor/core';
import { useIonRouter } from '@ionic/react';

const Home = ({data1,func,history}) => {
  const ionRouter = useIonRouter();

  useEffect(()=> {
    document.addEventListener('ionBackButton',(e) => {
      e.detail.register(-1,()=> {
        if(!ionRouter.canGoBack()) {
          Plugins.App.exitApp();
        }
      })
    })
  },[])
  return (
      <IonRouterOutlet>
        <Route path="/dash">
          <IonPage>
            <IonHeader>
              <IonToolbar>
                <IonTitle>Med App</IonTitle>
              </IonToolbar>
            </IonHeader>
          
            <IonContent fullscreen>
              <div style={{display:"flex",flexDirection:"column",alignItems:"center",}}>
                <IonList>
                  <IonItem>
                    <IonLabel>Name :</IonLabel>
                    <IonInput value={data1[0]} 
                      onIonChange={(e) =>{
                        let temp1 = Array.from(data1)
                        temp1[0] = e.detail.value
                        func(temp1)
                      }}
                    ></IonInput>
                  </IonItem>
                  <IonItem>
                    <IonLabel>Chemicals :</IonLabel>
                    <IonInput value={data1[1]} 
                      onIonChange={(e) =>{
                        let temp1 = Array.from(data1)
                        temp1[1] = e.detail.value
                        func(temp1)
                      }}
                    ></IonInput>
                  </IonItem>
                  <IonItem>
                    <IonLabel>Manufacturer :</IonLabel>
                    <IonInput value={data1[2]} 
                      onIonChange={(e) =>{
                        let temp1 = Array.from(data1)
                        temp1[2] = e.detail.value
                        func(temp1)
                      }}
                    ></IonInput>
                  </IonItem>
                  <IonItem>
                    <IonLabel>License :</IonLabel>
                    <IonInput value={data1[3]} 
                      onIonChange={(e) =>{
                        let temp1 = Array.from(data1)
                        temp1[3] = e.detail.value
                        func(temp1)
                      }}
                    ></IonInput>
                  </IonItem>
                <IonItemDivider></IonItemDivider>
                </IonList>
                <IonButton shape="round" fill="outline" style ={{margin : "10px"}}
                  onClick={e => {
                    e.preventDefault();
                    history.push("/dash/list")
                  }}>
                  Send
                </IonButton>
              </div>
            </IonContent>
          </IonPage>
        </Route>
        
      </IonRouterOutlet>
      
    );
  };
  
  export default Home;
  