import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet,IonBackButton,IonButtons,IonPage,IonHeader,IonText,IonTitle,IonToolbar,IonItem ,IonContent,IonList, IonButton, IonCard, IonCardContent, IonIcon, IonLabel, IonCardHeader, IonCardTitle, IonCardSubtitle} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './Home'
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import '../theme/variables.css';
import React, { useEffect, useState } from 'react';

const Dash = ({match}) => {

     
    const [obj,setObj] = useState(["","","",""])
    return (
        <IonRouterOutlet>
            <Route exact path="/dash"
            render = {(props)=> <Home {...props} func={setObj} data1={obj} />}             
            />
            <Route path="/dash/list" 
            render={(props) => <List {...props} data1={obj}/>}
            />
        </IonRouterOutlet>
        )
    }
const List = ({data1,match}) => {

    
    const [list1,setList1] = useState([{name: "loading"}])
    const [show,setShow] = useState({name:"loading"})
    useEffect(() => {   
        console.log(match.url)
        fetch(`https://med-digu.herokuapp.com/med`, {
            method: "PUT",
            body: JSON.stringify({
                "id" : -1 ,
                "name" :data1[0],
                "chem" : data1[1],
                "manu" : data1[2],
                "license" : data1[3],
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        }).then(res=> res.json()).then(json => setList1(json))
    },[])
    return (
        <IonRouterOutlet>
            <Route exact path="/dash/list">
                <IonPage>
                    <IonHeader>
                        <IonToolbar>
                            <IonButtons slot="start">
                                <IonBackButton />
                            </IonButtons>
                            <IonTitle>Medicines</IonTitle>
                        </IonToolbar>
                    </IonHeader>
                    <IonContent>
                        <IonList>
                            {list1.map((every)=> 
                                <IonItem 
                                    button onClick={() => setShow(every)}
                                    routerLink="/dash/list/detail"
                                >
                                    <IonText color="primary">
                                        <h3> {every.name}</h3>
                                    </IonText>
                                </IonItem>
                            )}
                        </IonList>
                    </IonContent>o
                </IonPage>
            </Route>
            <Route path="/dash/list/detail"
                render={(props) => <Detail {...props} data1={show}/>}
            />
        </IonRouterOutlet>
        
    )
}

const Detail = ({data1,match}) => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton />
                    </IonButtons>
                    <IonTitle>Detail</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonCard >
                    <img src={"data:image/;base64,"+data1.image}
                    /> 
                    <IonCardHeader>
                        <IonCardTitle>
                            {data1.name}
                        </IonCardTitle>
                        <IonCardSubtitle>
                            {data1.chemicals}
                        </IonCardSubtitle>
                    </IonCardHeader>
                    <IonCardContent>
                        {data1.price == -1 ? <p> Unknown Price </p> : <p> data1.price Tk</p>}  
                        {data1.license}
                    </IonCardContent>
                    
                </IonCard> 
                <IonCard >
                    <IonCardHeader>
                        <IonCardTitle>
                            Description
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        {data1.description}
                    </IonCardContent>
                    
                </IonCard>    
            </IonContent>
        </IonPage>
    )
}
        
export default Dash 